# Componentes e Códigos para o portal da FEI

- [Conheça o site &rarr;](http://portal.fei.edu.br)
- [Página com a documentação dos componentes](http://portal.fei.edu.br/componentes)

## Arquitetura CSS

Para nomear as classes, é utilizado a arquitetura [BEM CSS](http://getbem.com/) em sua totalidade, a inclusão da nomeclatura [SMACSS](https://smacss.com/) para classes de estado e alguns conceitos das [Sass Guidelines](https://sass-guidelin.es/pt/).

### Exemplo

(BEM) O código abaixo indica uma variação do design do botão:

```html
<button class="button button--primary">Primário</button>
```

(SMACSS) Diferente da indicação de alteração do botão, a baixo vemos que a navegação em aba possui uma classe `.is-active` para indicar o item selecionado.

```html
<ul class="fei-tabs">
  <li class="is-active"><a href="#" data-href="#..." data-toggle="tab-nav">...</a></li>
  <li><a href="#" data-href="#..." data-toggle="tab-nav">...</a></li>
  <li><a href="#" data-href="#..." data-toggle="tab-nav">...</a></li>
  ...
</ul>
```

## Transição

É possível notar nos arquivos que alguns componentes possuem duas nomeclaturas de classes CSS, isso acontece pois ainda estou fazendo uma migração do CSS puro para o uso do pré-processador Sass (com a syntax SCSS) e também a alteração da arquitetura dos componentes. Isso envolve um período onde os estilos estejam funcionais para as classes novas e antigas, até que tudo seja padronizado.