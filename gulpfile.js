var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var prefix = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var minifyCSS = require('gulp-clean-css');

var inputSCSS = 'src/sass/**/*.scss';
var imgSrc = 'src/img/**/*';

gulp.task('compileSCSS', function(){
	return gulp
	.src(inputSCSS)
	.pipe(sourcemaps.init())
	.pipe(sass({outputStyle: "expanded"}).on('error', sass.logError))
	.pipe(prefix({
		browsers: ['last 4 versions'],
		cascade: false,
	}))
	.pipe(sourcemaps.write('maps/'))
	.pipe(gulp.dest('dist/css'));
});

gulp.task('minifyCSS', function(){
	return gulp
	.src('dist/css/*.css')
	.pipe(minifyCSS({
		level: 0
	}))
	.pipe(rename({
		basename: 'style',
		suffix: '.min',
		extname: '.css'
	}))
	.pipe(gulp.dest('dist/css-min'));
});

gulp.task('watch', function(){
	gulp.watch(inputSCSS, ['compileSCSS']);
	gulp.watch('dist/css/*.css', ['minifyCSS']);
});

gulp.task('default', ['compileSCSS', 'minifyCSS', 'watch']);