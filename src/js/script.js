$(function() {
	var drag = false;

	function getTarget(obj) {
		var str = $(obj).attr('data-href');
		if (str.substring(0, 1) == "#") {
			return str;
		} else {
			return "#" + str;
		}
	}

/*
  - NAVEGAÇÃO ENTRE PAINEIS DE CONTEÚDO
	ATRAVÉS DO CONTROLE DE ABAS
	*/

	if (window.location.hash) {
		var target = window.location.hash;
		$(target).siblings().removeClass('is-active');
		$(target).addClass('is-active');
		$("[data-toggle='tab-nav'][data-href='" + target + "']").parent().siblings().removeClass('is-active');
		$("[data-toggle='tab-nav'][data-href='" + target + "']").parent().addClass('is-active');
	}

	$(document).on('click', "[data-toggle='tab-nav']", function(e) {
		e.preventDefault();
		var target = getTarget(this);
		$(target).siblings().removeClass('is-active');
		$(target).addClass('is-active');
		$("[data-toggle='tab-nav'][data-href='" + target + "']").parent().siblings().removeClass('is-active');
		$("[data-toggle='tab-nav'][data-href='" + target + "']").parent().addClass('is-active');
		if ($(target).hasClass('fei-painel-container')) {
			$('html, body').animate({
				scrollTop: $(target + '.is-active').offset().top,
			}, 600);
		}
		$('[data-slideshow]').flickity('resize');
	});

	$(document).on('click', "[data-toggle='accordion']", function(e) {
		e.preventDefault();
		var target = getTarget(this);
		$(target).toggleClass('is-active');
		$(this).toggleClass('is-active');
	});

/*
  - CLIQUE RANDOMICO NA SESSAO DE PROJETOS
  */

  /*$('.fei-projects .fei-tabs a').eq(Math.floor(Math.random() * 7)).click();*/

/*
  - AFFIX SCRIPT PARA AS ABAS LATERAIS
	DA PÁGINA DE DOCENTES E CORPO DIRETIVO
	*/

	$("[data-tab='affix']").affix({
		offset: {
			top: function() {
				return ($('.fei-painel-container.is-active').parent().offset().top);
			},
			bottom: function() {
				var all = $('.fei-painel-container.is-active').parents('.fei-section').nextAll();
				var elHeight = 0;
				for (i = 0; i < all.length; i++) {
					elHeight += $(all[i]).outerHeight(true);
				}
				return (elHeight + 50);
			}
		}
	});

	if ($("[data-tab='affix']").hasClass("affix"))
		$("[data-tab='affix']").css('width', $("[data-tab='affix']").parent().width());

	$("[data-tab='affix']").on('affix.bs.affix load', function() {
		$(this).css('width', $(this).parent().width());
	});

	$("[data-tab='affix']").on('affix-top.bs.affix affixed-bottom.bs.affix', function() {
		$(this).css('width', '');
	});

//--------------------------

/*
  - BOTÃO PARA ABRIR O MENU MOBILE
  */

  $(document).on('click', '[data-toggle-menu]', function() {
  	var target = $(this).attr('data-nav-target');
  	$(this).children('.fei-nav-icon').toggleClass('fei-nav-close');
  	$(target).slideToggle();
  });

//--------------------------

/*
  - BOTÃO PARA ABRIR O MENU RÁPIDO
  */

  $(document).on('click', '[data-quick-links]', function() {
  	$('.fei-quick-links').addClass('is-active');
  	$('.off-canvas').addClass('is-active');
  	$('body').addClass('open quick');
  });

//--------------------------

/*
	- SCRIPTS DO CAROUSEL -

  - INICIALIZAÇÃO DO CAROUSEL
  */

  $('[data-slideshow]').flickity({
  	cellAlign: 'center',
  	contain: true,
  	wrapAround: true,
  	pageDots: false,
  	prevNextButtons: false,
  	cellSelector: '.cell',
  	autoPlay: 8000,
  	lazyLoad: 1,
  	imagesLoaded: true,
  	adaptiveHeight: true,
  	groupCells: true
  });

/*
  - BOTÃO DIRECIONAL DE CONTROLE
	DO CAROUSEL
	*/

	$('.control.next').on('click', function() {
		$(this).parents('[data-slideshow]').flickity('next');
	});

	$('.control.prev').on('click', function() {
		$(this).parents('[data-slideshow]').flickity('previous');
	});

/*
  - NAVEGAÇÃO DOS SLIDES DO CAROUSEL
  */

  $("[data-slide]").on('click', function() {
  	var $carousel = $(this).parents('[data-slideshow]');
  	$carousel.flickity('selectCell', $carousel.find('[data-slide]').index(this));
  });

  $('[data-slideshow]').on('select.flickity', function() {
  	var flkty = $(this).data('flickity');
  	var slideIndex = flkty.selectedIndex;
  	var el = flkty.selectedElement;
  	$(this).find('[data-slide]')
  	.siblings().removeClass('is-active')
  	.end()
  	.eq(slideIndex).addClass('is-active');
  	$(this).find('.slideshow-legenda')
  	.text($(el).find('img').attr('alt'));
  });

//--------------------------

/*
  - MODAL
	BOTÕES PARA ABRIR E FECHÁ-LO

	O BOTÃO DE FECHAR PODE SER
	REUTILIZADO PRA ESTRUTURAS
	PARECIDAS COM O MODAL, QUE
	ADICIONEM A CLASE 'is-active'
	PARA MOSTRAR ALGO NA TELA
	*/

	$(document).on('click', "[data-target='modal']", function(e) {
		e.preventDefault();
		var modal = getTarget(this);
		$(modal).addClass('is-active');
		$("body").addClass("open");
	});

	$(document).on('click', "[data-dismiss='close']", function() {
		$('.off-canvas').removeClass('is-active')
		$('.off-canvas').siblings().removeClass('is-active');
		var el = $(this).parent();
		el.removeClass('is-active').addClass('is-hiding');
		setTimeout(function(){
			el.removeClass('is-hiding');
		}, 500);
		$("body").removeClass();
	});

//--------------------------

/*
  - CONTROLE DA VISUALIZAÇÃO
	DO BOTÃO 'VOLTAR AO TOPO'
	*/
	var scrPos = 0, alrt = 0;
	$(window).on('scroll', function() {
		var scrPosTop = $(this).scrollTop();
		if( scrPosTop > scrPos && alrt != 1){
			$('#alert-vestibular').addClass('is-active');
			$('.fei-sharelinks').removeClass('is-active');
		} else if(scrPosTop < scrPos && alrt != 1){
			$('#alert-vestibular').removeClass('is-active');
			$('.fei-sharelinks').addClass('is-active');
			alrt = 1;
		}
		if ($(this).scrollTop() > 500 && $(this).width() > 992) {
			$('.fei-back-top a').addClass('is-active');
		} else {
			$('.fei-back-top a').removeClass('is-active');
		}
		scrPos = scrPosTop;
	});

//--------------------------

/*
  - SCRIPT DE ANIMAÇÃO DA ROLAGEM
	DA PÁGINA
	*/

	$("a[href^='#']").click(function() {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				$('html, body').animate({
					scrollTop: target.offset().top
				}, 800);
				return false;
			}
		}
	});

/*
	- FILE UPLOAD
	*/

	$('input[type="file"]').on('change', function() {
		var fileName = $(this)[0].files[0].name;
		$(this).parent().find('.file-name').text(fileName);
	});

/*
  -  API DO YOUTUBE
  */

  var tag = document.createElement('script');
  tag.src = 'https://www.youtube.com/iframe_api';
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  var player;

  $("[data-target='youtube']").on('click', function() {
  	var video_id = $(this).attr('data-video-id');

  	player = new YT.Player('player', {
  		width: 480,
  		height: 270,
  		videoId: video_id,
  		playerVars: {
  			'autoplay': 1,
  		},
  		event: {
  			'onReady': onPlayerReady,
  			'onStateChange': onPlayerStateChange
  		}
  	});
  });

  function onPlayerReady(e) {
  	e.target.playVideo();
  }

  var done = false;

  function onPlayerStateChange(event) {
  	if (event.data == YT.PlayerState.PLAYING && !done) {
  		setTimeout(stopVideo, 6000);
  		done = true;
  	}
  }

  function stopVideo() {
  	player.stopVideo();
  }


  $('body').on('touchmove', function() {
  	drag = true;
  });
  $('body').on('touchstart', function() {
  	drag = false;
  });


  $('.fei-bnav-body > ul > li').on('touchend', function(e) {
  	if (drag)
  		return false;
	'use strict'; //satisfy code inspectors
	var link = $(this); //preselect the link
	if (link.hasClass('hover') || !link.children(".fei-navigation-dropdown").length)
		return true;
	else {
		link.toggleClass('hover');
		link.siblings().removeClass('hover');
		e.preventDefault();
		return false; //extra, and to make sure the function has consistent return points
	}
});

  [].forEach.call(document.querySelectorAll('img[data-src]'), function(img) {
  	img.setAttribute('src', img.getAttribute('data-src'));
  	img.onload = function() {
  		img.removeAttribute('data-src');
  	};
  });

  var lastId,
  sideMenu = $('.fei-scroll-tabs'),
  menuItems = sideMenu.find('a'),
  scrollItems = menuItems.map(function() {
  	var item = $($(this).attr('href'));
  	if (item.length) {
  		return item;
  	}
  });

  $(window).on('scroll', function() {
  	var fromTop = $(this).scrollTop() + 50;

  	var cur = scrollItems.map(function() {
  		if ($(this).offset().top < fromTop) {
  			return this;
  		}
  	});
  	cur = cur[cur.length - 1];
  	var id = cur && cur.length ? cur[0].id : "";

  	if (lastId != id) {
  		lastId = id;

  		menuItems
  		.parent().removeClass('is-active')
  		.end().filter("[href='#" + id + "']").parent().addClass('is-active');
  	}
  });

  $('.cpf').mask('000.000.000-00', {
  	placeholder: '___.___.___-__'
  });
  $('.cel').mask('(00) 00000-0000', {
  	placeholder: '(__) _____-____'
  });
  $('.tel').mask('(00) 0000-0000', {
  	placeholder: '(__) ____-____'
  });
  $('.date').mask('00/00/0000 00:00', {
  	placeholder: '__/__/____ __:__'
  });

  $('[data-input="radio"] input[type="radio"]').on('change', function(){
  	var i = 1;
  	var v = parseInt($(this).val());
  	$('[data-input="hidden"]').css('display', 'none').find('input.required').removeAttr('required');
  	$('[data-input="hidden"]').each(function(){
  		if(i <= v)
  			$(this).css('display', 'block').find('input.required').attr('required', 'true');
  		else
  			return;
  		i++;
  	});
  });

  $(document).on('click', "[data-action='newsletter']", function () {
  	var target = getTarget(this);
  	var alertDiv = $("[data-alert='message']");
  	$(this).prop("disabled",true);      
  	$.ajax({
  		type: "POST",
  		url: "/Home/NewsLetter",
  		contentType: "application/json",      
  		data: JSON.stringify({ contato: $(target).find('.fei-input-control').val() }),			
  		success: function (retorno) {
  			alertDiv.removeClass().addClass('fei-alert');
  			if (retorno.Status == "erro") {
  				alertDiv.addClass("alert-danger");
  			} else {
  				alertDiv.addClass("alert-success");
  			}
  			alertDiv.find(".alertText").html(retorno.Mensagem);
  			alertDiv.addClass('is-active');
  			$("[data-href='" + target + "']").prop("disabled",false);
  		},
  		error: function(err) { 
  			$("[data-href='" + target + "']").prop("disabled",false);
  		}
  	}); 
  	setTimeout(function(){
  		alertDiv.removeClass('is-active');
  	}, 15000);
  });

  $('.fei-sharelinks').on('click', '.facebook-share', function(e){
  	e.preventDefault();
  	var url = 'http://www.facebook.com/share.php?u=' + window.location;
  	window.open(url, 'facebook-share', 'width=550,height=550');
  });

  $('.fei-sharelinks').on('click', '.twitter-share', function(e){
  	e.preventDefault();
  	var url = 'https://twitter.com/intent/tweet?text=' + document.title + '&url=' + window.location + '&via=FEIonline';
  	window.open(url, 'twitter-share', 'width=550,height=550');
  });

  $('.fei-sharelinks').on('click', '.whatsapp-share', function(e){
  	e.preventDefault();
  	window.location.href = 'https://api.whatsapp.com/send?text=' + document.title + ' ' + window.location;
  });

  $('.fei-sharelinks').on('click', '.gplus-share', function(e){
  	e.preventDefault();
  	var url = 'https://plus.google.com/share?url=' + window.location;
  	window.open(url, 'gplus-share', 'width=550,height=550');
  });

  $('.fei-sharelinks').on('click', '.linkedin-share', function(e){
  	e.preventDefault();
  	var url = 'https://www.linkedin.com/shareArticle?url=' + window.location + '&title=' + document.title + '&source=FEI';
  	window.open(url, 'gplus-share', 'width=550,height=550');
  });

  $('.fei-sharelinks').on('click', '.clipboard', function(e){
  	e.preventDefault();

  	var p = document.createElement("p");
  	p.append(window.location);
  	$('body').append(p);

  	var selection = window.getSelection();
  	var range = document.createRange();
  	range.selectNodeContents(p);
  	selection.addRange(range);

  	try{
  		var scs = document.execCommand('copy');
  		var msg = scs ? "Link copiado!\n" + window.location : "Erro ao copiar";
  		alert(msg);
  	} catch(err){
  		alert("Erro inesperado!");
  	}

  	selection.removeAllRanges();
  	$('body').removeChild(p);
  });
});